//
// Created by ASUS on 26/09/2020.
//

#include<bits/stdc++.h>
#define ll long long int
using namespace std;

ll modulo(ll x, ll y, long p) {
    if (x == 0) return 0;
    if (y == 0) return 1;
    ll res = modulo(x, y / 2, p);
    if (y % 2 == 0) return (res * res) % p;
    return (res * res % p) * x % p % p;
}

int main() {
    int t;
    int x, y;
    long long p;
    cin >> t;
    while(t--) {
        cin >> x >> y >> p;
        cout << modulo(x, y, p) << endl;
    }
    return 0;
}
