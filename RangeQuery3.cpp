#include<bits/stdc++.h>
using namespace std;

int a[100000], i, j;
void sieve() {
    for (i = 2; i <= 100000; ++i) a[i] = 1;
    for (i = 2; i * i <= 100000; ++i) {
        if (a[i]) {
            for (j = i * i; j <= 100000; j += i) {
                a[j] = 0;
            }
        }
    }
}

int main() {
    int t, l, r;
    cin >> t;
    sieve();
    while (t--){
        cin >> l >> r;
        int count = 0;
        for (i = l; i <= r; ++i) {
            if (a[i]) ++count;
        }
        cout << count << endl;
    }
}
