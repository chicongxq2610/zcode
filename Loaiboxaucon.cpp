//
// Created by ASUS on 25/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

string removeWord(string str, string word) {
    if (str.find(word) != string::npos) {
        size_t p = -1;
        while((p = str.find(word)) != string::npos) {
            str.replace(p - 1, word.length() + 1, "");
        }
    }
    return str;
}

int main() {
    string str, word;
    getline(cin, str);
    getline(cin, word);
    cout << removeWord(str, word);
    return 0;
}