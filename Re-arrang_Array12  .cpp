//
// Created by ASUS on 07/12/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        int a[n+1];
        long long max = INT_MIN;
        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }
        for (int i = 0; i < n; i++) {
            long long mul = 1;
            for (int j = i; j < n; j++) {
                mul *= a[j];
                if (mul > max) max = mul;
            }
        }
        cout << max << endl;
    }
    return 0;
}