//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    long long t, n, i, k, a[100000];
    cin >> t;
    while (t--) {
        cin >> n >> k;
        for (i = 0; i < n; ++i) cin >> a[i];
        sort(a, a + n);
        for (i = n - 1; i >= n - k; --i) cout << a[i] << " ";
        cout << endl;
    }
    return 0;
}