//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

void modifyMatrix(int a[][100], int n, int m) {
    bool row[n] , col[m];
    int i, j;
    for (i = 0; i < n; ++i) row[i] = 0;
    for (j = 0; j < m; ++j) col[j] = 0;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < m; ++j) {
            if (a[i][j] == 1) {
                row[i] = 1;
                col[j] = 1;
            }
        }
    }
    for (i = 0; i < n; ++i) {
        for ( j = 0; j < m; ++j) {
            if (row[i] == 1 || col[j] == 1) {
                a[i][j] = 1;
            }
        }
    }
    for (i = 0; i < n; ++i) {
        for (j = 0; j < m; ++j) {
            cout << a[i][j] << " ";
        }
        cout << endl;
    }
}

int main() {
    int t, n, m, i, j, a[100][100];
    cin >> t;
    while (t--) {
        cin >> n >> m;
        for (i = 0; i < n; ++i) {
            for (j = 0; j < m; ++j) {
                cin >> a[i][j];
            }
        }
        modifyMatrix(a, n, m);
    }
    return 0;
}