#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, m, n;
    cin >> t;
    while (t--){
        cin >> n >> m;
        int a[n+m], b[100000] = {0};
        for (int i = 0; i < n + m; i++) {
            cin >> a[i];
            b[a[i]]++;
        }
        for (int i = 0; i < 100000; i++) {
            if (b[i] != 0) cout << i << " ";
        }
        cout << endl;
        for (int i = 0; i < 100000; i++) {
            if (b[i] > 1) cout << i << " ";
        }
        cout << endl;
    }
}
