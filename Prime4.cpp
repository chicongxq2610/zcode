#include<bits/stdc++.h>
using namespace std;

void printPrimeFactors(int n) {
    if (n % 2 == 0) {
        cout << "2 ";
        return;
    }
    for (int i = 3; i <= sqrt(n); i += 2) {
       if (n % i == 0) {
           cout << i << " ";
           return;
       }
    }
    if (n > 2) {
        cout << n << " ";
        return;
    }
}

int main() {
    int t, n, k;;
    cin>>t;
    while(t--) {
        cin >> n;
        cout << "1 ";
        for (k = 2; k <= n; ++k) printPrimeFactors(k);
        cout << endl;
    }

    return 0;
}

