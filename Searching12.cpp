//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, x;
    cin >> t;
    while (t--) {
        cin >> n >> x;
        int a[n];
        set<int> mySet;
        for (i = 0; i < n ; i++) {
            cin >> a[i];
            mySet.insert(a[i]);
        }
        int flag = -1;
        for (int i = 0; i < n; i++) {
            if (mySet.find(x + a[i]) != mySet.end() || mySet.find(abs(x-a[i])) != mySet.end()) {
                flag = 1;
                break;
            }
        }
        cout << flag << endl;
    }
    return 0;
}