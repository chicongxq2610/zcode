//
// Created by ASUS on 04/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i;
    cin >> t;
    while (t--) {
        cin >> n;
        int a[n+1];
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < n; i++) {
            if (a[i]  == a[i+1]) {
                a[i] = a[i] * 2;
                a[i+1] = 0;
            }
        }
        int count = 0;
        for (i = 0; i < n; ++i) {
            if (a[i] != 0) {
                cout << a[i] << " ";
                count++;
            }
        }
        for (i = 0; i < n - count; ++i) {
            cout << 0 << " ";
        }
        cout << endl;
    }
    return 0;
}