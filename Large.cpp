//
// Created by ASUS on 25/09/2020.
//
#include <bits/stdc++.h>
using namespace std;

string totalLargeNumber (string str1, string str2) {
    string str = "";
    int sub = 0, carry = 0;
    int len1 = str1.length(), len2 = str2.length();
    reverse(str1.begin(), str1.end());
    reverse(str2.begin(), str2.end());
    for (int i = 0; i < len2; i++) {
        sub = (str1[i] - '0') + (str2[i] - '0') + carry;
        if (sub > 10) {
            sub -= 10;
            carry = 1;
        }
        else carry = 0;
        str.push_back(sub + '0');
    }
    for (int i = len2; i < len1; i++) {
        sub = (str1[i] - '0') + carry;
        if (sub > 10) {
            sub -= 10;
            carry = 1;
        }
        else carry = 0;
        str.push_back(sub + '0');
    }
    reverse(str.begin(), str.end());
    return str;
}

int main() {
    int t;
    string str, str1, str2;
    cin >> t;
    while (t--) {
        cin >> str1 >> str2;
        str = totalLargeNumber(str1, str2);
        cout << str << endl;
    }
    return 0;
}
