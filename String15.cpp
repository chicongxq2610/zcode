//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, i, j;
    string s;
    cin >> t;
    while (t--) {
        cin.ignore();
        cin >> s;
        int len = s.length(), count = 0;
        for (i = 0; i < len; ++i) {
            for (j = i; j < len; ++j) {
                if (s[i] == s[j]) count++;
            }
        }
        cout << count << endl;
    }
    return 0;
}