//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, a, m, i, flag;
    cin >> t;
    while (t--) {
        cin >> a >> m;
        flag = 0;
        for (i = 0; i < m; ++i) {
            if ((a * i) % m == 1) {
                cout << i << endl;
                flag = 1;
                break;
            }
        }
        if (!flag) cout << -1 << endl;
    }
    return 0;
}
