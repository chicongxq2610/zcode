//
// Created by ASUS on 15/01/2021.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--) {
        string s;
        int k, mark[26], count, ans = 0;
        cin >> s >> k;
        for (int i = 0; i < s.length(); i++) {
            memset(mark, 0 , sizeof(mark));
            count = 0;
            for (int j = i; j < s.length(); j++) {
                if (mark[s[j] - 'a'] == 0) count++;
                mark[s[j] - 'a']++;
                if (count == k) ans++;
                if (count > k) break;
            }
        }
        cout << ans << endl;
    }
    return 0;
}