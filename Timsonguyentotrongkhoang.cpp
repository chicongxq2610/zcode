//
// Created by ASUS on 26/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

void sieveOfEratosthenes(long long a, long long b) {
    long long arr[b+1];
    for (long long i = 2; i <= b; i++) arr[i] = 1;
    for (long long i = 2; i * i <= b; i++) {
        if (arr[i]) {
            for (long long j = i * i; j <= b; j += i) {
                arr[j] = 0;
            }
        }
    }
    for (long long i = a; i <= b; i++)
        if (arr[i]) cout << i << " ";
}

int main() {
    int a, b;
    cin >> a >> b;
    if (a > b) swap(a, b);
    sieveOfEratosthenes(a, b);
    return 0;
}