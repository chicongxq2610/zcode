#include<bits/stdc++.h>
using namespace std;

long long solve(int arr[], int n) {
    long long incl = arr[0];
    long long excl = 0;
    long long excl_new;
    int i;

    for (i = 1; i < n; i++) {
        excl_new = (incl > excl)? incl: excl;
        incl = excl + arr[i];
        excl = excl_new;
    }
    return ((incl > excl)? incl : excl);
}
int main() {
    int t, n;
    cin >> t;
    while (t--){
        cin >> n;
        int a[n+1];
        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }
        cout << solve(a, n) << endl;
    }
}
