//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    long long t, n, i, k, count, a[100000];
    cin >> t;
    while (t--) {
        count = 0;
        cin >> n >> k;
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < n; ++i) {
            if (a[i] == k) count++;
        }
        if (count != 0) cout << count;
        else cout << -1;
        cout << endl;
    }
    return 0;
}