//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int solve(int arr1[], int arr2[], int n) {
    int maxLen = 0, len  = 0;
    for (int i=0; i<n; i++) {
        int sum1 = 0, sum2 = 0;
        for (int j=i; j<n; j++) {
            sum1 += arr1[j];
            sum2 += arr2[j];

            if (sum1 == sum2) 
                len = j-i+1;
            if (len > maxLen)
                    maxLen = len;
        }
    }
    return maxLen;
}

int main() {
    int t, n, i, a1[1000], a2[1000];
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a1[i];
        for (i = 0; i < n; ++i) cin >> a2[i];
        cout << solve(a1, a2, n) << endl;
    }
    return 0;
}