#include<bits/stdc++.h>
using namespace std;
typedef unsigned long int uli;
uli gcd(uli a, uli b) {
    if (a == 0) return b;
    return gcd(b % a, a);
}
uli lcm(uli a, uli b) {
	return (a * b) / gcd(a, b);
}
int main() {
	int t;
	cin>>t;
	while(t--) {
		uli n;
		cin>>n;
		uli result = 1;
		for(uli i = 1; i <= n; i++) {
			result = lcm(result, i);
		}
		cout<<result;
	}
	return 0;
}

