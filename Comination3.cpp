//
// Created by ASUS on 05/12/2020.
//

#include<bits/stdc++.h>
#define MAX 20
using namespace std;

int n, a[MAX], Bool[MAX] = {0};

void print() {
    for (int i = 1; i <= n; i++) {
        cout << a[i];
    }
    cout << " ";
}

void Solve(int k) {
    for (int i = 1; i <= n; i++) {
        if (!Bool[i]) {
            a[k] = i;
            Bool[i] = 1;
            if (k == n) print();
            else Solve(k + 1);
            Bool[i] = 0;
        }
    }
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        cin >> n;
        Solve(1);
        cout << endl;
    }
    return 0;
}