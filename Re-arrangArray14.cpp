//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, k, n, a[10000], i, j, tp, m;
    cin >> t;
    while (t--) {
        cin >> k >> n;
        m = k * n;
        for (i = 0; i < m; ++i) cin >> a[i];
        sort(a, a + m);
        for (i = 0; i < m; ++i) cout << a[i] << " ";
        cout << endl;
    }
    return 0;
}