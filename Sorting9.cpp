//
// Created by ASUS on 04/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, k, i, j, count, a[100];;
    cin >> t;
    while (t--) {

        cin >> n >> k;
        for (i = 0; i < n; ++i) cin >> a[i];
        count = 0;
        for (i = 0; i < n; ++i)
            for (j = i + 1; j < n; ++j)
                if (a[i] + a[j] == k)
                    count++;
        cout << count << endl;
    }
    return 0;
}