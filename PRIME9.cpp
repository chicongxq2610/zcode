#include<bits/stdc++.h>
using namespace std;

void printPrimeFactors(long long n) {
	long long count = 0;
	while (n % 2 == 0) {
		n = n / 2;
		count++;
	}
	if (count != 0) cout<<"2 "<<count<<" ";
	for (long long i = 3; i <= sqrt(n); i += 2) {
		count = 0;
		while (n % i == 0) {
			n = n / i;
			count++;
		}
		if (count != 0) cout<<i<<" "<<count<<" ";
	}
	if (n > 2) cout<<n<<" 1";
}

int main() {
	int t;
	cin>>t;
	while(t--) {
		long long n;
		cin>>n;
		printPrimeFactors(n);
		cout<<endl;
	}

	return 0;
}

