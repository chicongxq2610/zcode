//
// Created by ASUS on 25/09/2020.
//
#include <bits/stdc++.h>
using namespace std;

int compareNumber (string s1, string s2) {
    int len1 = s1.length(), len2 = s2.length();
    if (len1 > len2) return 1;
    if (len1 < len2) return -1;
    for (int i = 0; i < len1; i++) {
        if(s1[i] > s2[i]) return 1;
        if(s1[i] < s2[i]) return -1;
    }
    return 0;
}

string atractLargeNumber (string s1, string s2) {
    string str = "";
    int a, b = 0;
    if (compareNumber(s1, s2) < 0) swap(s1, s2);
    if (compareNumber(s1, s2) == 0) return "0";
    int len1 = s1.length(), len2 = s2.length();
    reverse(s1.begin(), s1.end());
    reverse(s2.begin(), s2.end());
    for (int i = 0; i < len2; i++) {
        a = (s1[i] - '0') - (s2[i] - '0') - b;
        if (a < 0) {
            a += 10;
            b = 1;
        }
        else b = 0;
        str.push_back(a + '0');
    }
    for (int i = len2; i < len1; i++) {
        a = (s1[i] - '0') - b;
        if (a < 0) {
            a += 10;
            b = 1;
        }
        else b = 0;
        str.push_back(a + '0');
    }
    reverse(str.begin(), str.end());
    return str;
}

int main() {
    int t;
    string str1, str2;
    cin >> t;
    while (t--) {
        cin >> str1 >> str2;
        cout << atractLargeNumber(str1, str2) << endl;
    }
    return 0;
}
