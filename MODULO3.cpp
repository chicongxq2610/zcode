//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i;
    long long k, s;
    cin >> t;
    while (t--) {
        s = 0;
        cin >> n >> k;
        for (i = 1; i <= n; ++i) {
            s += i % k;
        }
        if (s == k) cout << 1;
        else cout << 0;
        cout << endl;
    }
    return 0;
}