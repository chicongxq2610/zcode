//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, i;
    string s;
    cin >> t;
    while (t--) {
        cin.ignore();
        getline(cin, s);
        int len = s.length(), count = 1;
        for (i = 0; i < len; ++i) {
            if (s[i] >= 'a' && s[i] <= 'z' && (s[i+1] == ' ' || s[i+1] == '\t' || s[i+1] == '\n'))
                count++;
        }
        cout << count << endl;
    }
    return 0;
}