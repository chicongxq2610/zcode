//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n1, n2, n3, i, j;
    cin >> t;
    while (t--) {
        cin >> n1 >> n2 >> n3;
        long long a[n1], b[n2], c[n3], d[1000000], k = 0;
        for (i = 0; i < n1; i++) cin >> a[i];
        for (i = 0; i < n2; i++) cin >> b[i];
        for (i = 0; i < n3; i++) cin >> c[i];
        for (i = 0; i < n1; i++) {
            for (j = 0; j < n2; j++) {
                if (a[i] == b[j]) d[k++] = a[i];
            }
        }
        for (i = 0; i < k; i++) {
            for (j = 0; j < n3; j++) {
                if (d[i] == c[j]) cout << d[i] << " ";
            }
        }
    }
    return 0;
}