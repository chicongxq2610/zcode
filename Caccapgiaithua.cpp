//
// Created by ASUS on 10/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, mark[10] = {0};
    string s;
    cin >> t;
    while (t--) {
        cin >> n;
        cin.ignore();
        cin >> s;
        for (i = 0; i < n; ++i) {
            switch (s[i]) {
                case '2':
                    mark[2]++;
                    break;
                case '3':
                    mark[3]++;
                    break;
                case '4':
                    mark[3]++;
                    mark[2] += 2;
                    break;
                case '5':
                    mark[5]++;
                    break;
                case '6':
                    mark[5]++;
                    mark[3]++;
                    break;
                case '7':
                    mark[7]++;
                    break;
                case '8':
                    mark[7]++;
                    mark[2] += 3;
                    break;
                case '9':
                    mark[7]++;
                    mark[2]++;
                    mark[3] += 2;
                    break;
            }
        }
        for (i = 9; i >= 0; --i) {
            if (mark[i]) {
                while(mark[i]) {
                    cout << i;
                    mark[i]--;
                }
            }
        }
        cout << endl;
    }
    return 0;
}