#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, k, n;
    cin >> t;
    while (t--){
        cin >> n >> k;
        int A[n], F[n];
        for (int i = 0; i < n; i++) {
            cin >> A[i];
            F[i] = 0;
        }
        sort(A, A+n);
        long long local = 0, ans = 0;
        for(int i=0; i<n; i++){
            F[i] = (i == 0)? 0 : F[i-1]-1;
            for(int j = local+1; j<n; j++){
                if(A[j] - A[i] >= k) break;
                else{
                    local++; F[i]++;
                }
            }
            ans += F[i];
        }
        cout << ans << endl;
    }
}
