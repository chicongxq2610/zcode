//
// Created by ASUS on 17/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, m, n, a, b, count, i;
    cin >> t;
    while (t--) {
        count = 0;
        cin >> m >> n >> a >> b;
        for (i = m; i <= n; ++i) {
            if (i % a == 0 || i % b == 0) count++;
        }
        cout << count << endl;
    }
    return 0;
}