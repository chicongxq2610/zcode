//
// Created by ASUS on 05/12/2020.
//

#include<bits/stdc++.h>
using namespace std;
typedef long long int ll;

ll modulo11(long long a, long long b, long long c) {
    if (b == 0) return 0;
    ll res = modulo11(a, b/2, c);
    if (b % 2 == 1)
        return (a % c +  2 * (res % c)) % c;
    else
        return (2 * (res % c)) % c;
}

int main() {
    int t;
    long long a, b, c;
    cin >> t;
    while (t--) {
        cin >> a >> b >> c;
        cout << modulo11(a, b, c) << endl;
    }
    return 0;
}