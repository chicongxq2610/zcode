//
// Created by ASUS on 09/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

void findSmallestandLargest (int m, int s) {
    if (s == 0) {
        if (m == 1) cout << "0 0";
        else cout << "-1 -1";
        return;
    }
    if (s > 9*m) {
        cout << "-1 -1";
        return;
    }
    int res[m], i, res1[m];
    int s1 = s;
    // Smallest
    s--;
    for (i = m - 1; i > 0; --i) {
        if (s > 9) {
            res[i] = 9;
            s -= 9;
        }
        else {
            res[i] = s;
            s = 0;
        }
    }
    res[0] = s + 1;
    for (i = 0; i < m; ++i) cout << res[i];
    cout << " ";
    //Largest
    for (i = 0; i < m; ++i) {
        if (s1 >= 9) {
            res1[i] = 9;
            s1 -= 9;
        }
        else {
            res1[i] = s1;
            s1 = 0;
        }
    }
    for (i = 0; i < m; ++i) cout << res1[i];
}

int main() {
    int m, s;
    cin >> m >> s;
    findSmallestandLargest(m, s);
    return 0;
}