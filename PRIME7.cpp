//
// Created by ASUS on 17/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int IsSphenic(int n) {
    int count = 0;
    for (int i = 2; i <= sqrt(n); i++){
        if ( n % i == 0){
            n /= i;
            count++;
        }
        if ( n % i == 0) return 0;
    }
    if ( n != 1 ) count++;
    return count == 3;
}

int main() {
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        if (IsSphenic(n)) cout << 1;
        else cout << 0;
        cout << endl;
    }
    return 0;
}