//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, q, i, l, r, j, res, a[10000];
    cin >> t;
    while (t--) {
        cin >> n >> q;
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < q; ++i) {
            res = 0;
            cin >> l >> r;
            for (j = l - 1; j < r; ++j) {
                res += a[j];
            }
            cout << res << endl;
        }
        cout << endl;
    }
    return 0;
}