//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, i, n, x, a[10000];
    cin >> t;
    while (t--) {
        cin >> n >> x;
        int flag = 0;
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < n; ++i) {
            if (a[i] == x) {
                cout << i + 1;
                flag = 1;
                break;
            }
        }
        if (!flag) cout << "-1";
        cout << endl;
    }
    return 0;
}