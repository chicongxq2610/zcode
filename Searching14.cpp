//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;
int a[1000000];
void findFirstRepeating(int a[], int n) {
    int min = -1;
    set<int> mySet;
    for (int i = 0; i < n; ++i) {
        if (mySet.find(a[i]) != mySet.end()) {
            min = i;
            break;
        }
        else mySet.insert(a[i]);
    }
    if (min != -1) cout << a[min];
    else cout << min;
}

int main() {
    int t, n, i;
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        findFirstRepeating(a, n);
        cout << endl;
    }
    return 0;
}