//
// Created by ASUS on 17/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int a[100000];
void SieveOfEratosthenes() {
    for (int i = 2; i <= 100000; i++) {
        a[i] = 1;
    }
    for (int i = 2; i * i <= 100000; i++) {
        if (a[i]) {
            for (int j = i * i; j <= 100000; j += i) {
                a[j] = 0;
            }
        }
    }
}

int check(long long n) {
    int count = 0;
    for (int i = 0; i <= sqrt(n); ++i) {
        if (a[i]) count++;
    }
    return count;
}

int main() {
    int t;
    long long n;
    cin >> t;
    SieveOfEratosthenes();
    while (t--) {
        cin >> n;
        cout << check(n) << endl;
    }
    return 0;
}