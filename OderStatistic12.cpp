//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int findMissingPositive(int a[], int n) {
    set<int> mySet;
    int m = 1, i;
    for (i = 0; i < n; ++i) {
        if (m < a[i]) mySet.insert(a[i]);
        else if (m == a[i]) {
            m++;
            while (mySet.count(m)) {
                mySet.erase(m);
                m++;
            }
        }
    }
    return m;
}

int main() {
    int t, a[10000], i, n;
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        cout << findMissingPositive(a, n) << endl;
    }
    return 0;
}