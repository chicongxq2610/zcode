//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, j;
    cin >> t;
    while (t--) {
        cin >> n;
        long long a[n+1], b[n+1];
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < n; ++i) b[i] = 0;
        j = 0;
        for (i = 0; i < n; ++i) {
            if (a[i] != 0) b[j++] = a[i];
        }
        for (i = 0; i < n; ++i) {
            cout << b[i] << " ";
        }
        cout << endl;
    }
    return 0;
}