#include <bits/stdc++.h>
using namespace std;

int main(){
    int t;
    cin>>t;
    getchar();
    while(t--){
        char S1[1005],b1[50][50], b2[50][50], S2[1005];
        cin.getline(S1,1005);
        cin.getline(S2,1005);
        int t1=0;
        char *token1=strtok(S1, " ");
        while (token1!=NULL) {
            strcpy(b1[t1++],token1);
            token1=strtok(NULL, " ");
        }
        int t2=0;
        char *token2=strtok(S2, " ");
        while (token2!=NULL) {
            strcpy(b2[t2++],token2);
            token2=strtok(NULL, " ");
        }
        for (int i = 0; i < t1; i++) {
            for (int j = 0; j < t2; j++) {
                if (strcmp(b1[i], b2[j]) != 0) cout << b1[i] << " ";
            }
        }
        cout<<endl;
    }
}