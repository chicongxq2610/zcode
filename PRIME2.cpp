#include<bits/stdc++.h>
using namespace std;

void printPrimeFactors(long long n) {
	long long max = 0;
	while (n % 2 == 0) {
		n = n / 2;
		max = 2;
	}
	for (long long i = 3; i <= sqrt(n); i += 2) {
		while (n % i == 0) {
			n = n / i;
			max = i;
		}
	}
	if (n > max) cout<<n;
	else cout<<max;
}

int main() {
	int t;
	cin>>t;
	while(t--) {
		long long n;
		cin>>n;
		printPrimeFactors(n);
		cout<<endl;
	}

	return 0;
}

