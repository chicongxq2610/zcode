//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t;
    long long n;
    cin >> t;
    while (t--) {
        cin >> n;
        if (n % 100 == 86)  cout << 1;
        else cout << 0;
        cout << endl;
    }
    return 0;
}