#include<bits/stdc++.h>
using namespace std;
int main() {
    int t, n, k;
    cin >> t;
    while (t--){
        cin >> n >> k;
        int a[n*n+1];
        for(int i = 0; i < n*n; ++i) {
            cin >> a[i];
        }
        sort(a, a+n*n);
        cout << a[k-1] << endl;
    }
}
