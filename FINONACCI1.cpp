//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
const long long mod = 1000000007;
using namespace std;

long long fib(long long n)
{
    long long a = 0, b = 1, c, i;
    if( n == 0)
        return a;
    for(i = 2; i <= n; ++i)
    {
        c = (a + b) % mod;
        a = b;
        b = c;
    }
    return b;
}


int main() {
    long long t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        cout << fib(n) << endl;
    }
    return 0;
}