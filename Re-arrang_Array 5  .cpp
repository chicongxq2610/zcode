//
// Created by ASUS on 04/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, a[10000];
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        sort(a, a + n);
        for (i = 0; i < n - i; ++i) {
            cout << a[n - i - 1] << " ";
            if (i != n / 2) cout << a[i] << " ";
        }
        cout << endl;
    }
    return 0;
}