#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n;
    cin >> t;
    while (t--){
        cin >> n;
        int a[n + 1];
        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }
        long long max = INT16_MIN, sum = 0;
        for (int i = 0; i < n; i++) {
            sum += a[i];
            if (max < sum) max = sum;
            if (sum < 0) sum = 0;
        }
        cout << max << endl;
    }
}
