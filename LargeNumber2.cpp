//
// Created by ASUS on 25/09/2020.
//
#include <bits/stdc++.h>
using namespace std;

string sumLargeNumber (string str1, string str2) {
    string str = "";
    int sum, carry = 0;
    if (str1.length() > str2.length()) swap(str1, str2);
    int len1 = str1.length(), len2 = str2.length();
    reverse(str1.begin(), str1.end());
    reverse(str2.begin(), str2.end());
    for (int i = 0; i < len1; i++) {
        sum = (str1[i] - '0') + (str2[i] - '0') + carry;
        str.push_back(sum%10 + '0');
        carry = sum/10;
    }
    for (int i = len1; i < len2; i++) {
        sum = (str2[i] - '0') + carry;
        str.push_back(sum%10 + '0');
        carry = sum/10;
    }
    if (carry) str.push_back(carry + '0');
    reverse(str.begin(), str.end());
    return str;
}

int main() {
    int t;
    string str, str1, str2;
    cin >> t;
    while (t--) {
        cin >> str1 >> str2;
        str = sumLargeNumber(str1, str2);
        cout << str << endl;
    }
    return 0;
}
