//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, min, a[1000];
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        sort(a, a+n);
        min = a[1] - a[0];
        for (i = 2; i < n; ++i) {
            if (a[i] - a[i - 1] < min) min = a[i] - a[i - 1];
        }
        cout << min << endl;
    }
    return 0;
}
