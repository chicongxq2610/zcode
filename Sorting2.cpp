#include<bits/stdc++.h>
using namespace std;
int x;
void swap(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(int arr[], int n) {
    int i, j;
    for (i = 0; i < n-1; i++)
        for (j = 0; j < n-i-1; j++)
            if (abs(x - arr[j]) > abs(x - arr[j + 1]))
                swap(&arr[j], &arr[j+1]);
}

int main() {
    int t, n;
    cin >> t;
    while (t--){
        cin >> n >> x;
        int a[n+1];
        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }
        bubbleSort(a, n);
        for (int i = 0; i < n; i++) {
            cout <<  a[i] << " ";
        }
        cout << endl;
    }
}
