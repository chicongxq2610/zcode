//
// Created by ASUS on 17/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

bool a[10000];

void SieveOfEratosthenes() {
    int i, j;
    for (i = 2; i <= 10000; ++i) a[i] = 1;
    for (i = 2; i <= sqrt(10000); ++i) {
        if (a[i]) {
            for (j = i * i; j <= 10000; j += i) {
                a[j] = 0;
            }
        }
    }
}

int main() {
    int t, m, n, i;
    cin >> t;
    SieveOfEratosthenes();
    while (t--) {
        cin >> m >> n;
        for ( i = m; i <= n; ++i) {
            if (a[i]) cout << i << " ";
        }
        cout << endl;
    }
    return 0;
}