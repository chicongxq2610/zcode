//
// Created by ASUS on 26/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

void toLowerString(string &str) {
    for (int i = 0; i < str.length(); i++) {
        if (str[i] >= 'A' && str[i] <= 'Z') str[i] += 32;
    }
}

vector<string> split(string str, string regex, int &count){
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    vector<string> arr;
    current=strtok(cstr,regex.c_str());
    while(current != NULL){
        count++;
        arr.push_back(current);
        current=strtok(NULL, regex.c_str());
    }
    return arr;
}
int main() {
    string str;
    int count = 0;
    vector<string> words;
    getline(cin, str);
    toLowerString(str);
    words = split(str, " ", count);
    cout << words[count-1];
    for (int i = 0; i < count - 1; i++) {
        cout<<words[i][0];
    }
    cout << "@ptit.edu.vn";
    return 0;
}