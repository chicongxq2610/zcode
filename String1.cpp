//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

bool checkPangram(string s, int k) {
    if (s.length() < 26) return false;
    bool mark[26];
    int i, count = 0;
    for (i = 0; i < 26; ++i) mark[i] = false;
    for (i = 0; i < s.length(); ++i) {
        mark[s[i] - 'a'] = true;
    }
    for (i = 0; i < 26; ++i) {
        if (!mark[i]) count += 1;
    }
    if (k >= count) return true;
    return false;
}

int main() {
    int t;
    int k;
    string s;
    cin >> t;
    while (t--) {
        cin.ignore();
        getline(cin, s);
        cin >> k;
        checkPangram(s, k) ? cout << "1\n" :
        cout<< "0\n";
    }
    return 0;
}