//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

void minAbsSumPair(int arr[], int arr_size) {
    int l, r, min_sum, sum;
    min_sum = arr[0] + arr[1];

    for(l = 0; l < arr_size - 1; l++) {
        for(r = l + 1; r < arr_size; r++) {
            sum = arr[l] + arr[r];
            if(abs(min_sum) > abs(sum)) {
                min_sum = sum;
            }
        }
    }
    cout << min_sum;
}

int main() {
    int t, n, i, a[100000];
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        minAbsSumPair(a, n);
        cout << endl;
    }
    return 0;
}