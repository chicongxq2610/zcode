//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, k, n, a[10000], i;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        for (i = 0; i < n; ++i) cin >> a[i];
        sort(a, a + n);
        cout << a[k-1] << endl;
    }
    return 0;
}