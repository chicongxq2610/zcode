#include<bits/stdc++.h>
using namespace std;

void SieveOfEratosthenes(int n) {
	int a[n+1];
	for (int i = 2; i <= n; i++) {
		a[i] = 1;
	}
	for (int i = 2; i * i <= n; i++) {
		if (a[i]) {
			for (int j = i * i; j <= n; j += i) {
				a[j] = 0;
			}	
		}
	}
	for (int i = 2; i <= n; i++) {
		if (a[i]) cout<<i<<" ";
	}
}

int main() {
	int n, t;
	cin>>t;
	while (t--) {
		cin>>n;
		SieveOfEratosthenes(n);
		cout<<endl;
	}

	return 0;
}

