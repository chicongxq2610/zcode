//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    long long t, n, i, a[100000];
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        sort(a, a + n);
        if (a[n - 1] == a[0]) cout << -1;
        else {
            cout << a[0] << " ";
            for (i = 1; i < n; ++i) {
                if (a[i] != a[i - 1]) {
                    cout << a[i];
                    break;
                }
            }
        }
        cout << endl;
    }
    return 0;
}