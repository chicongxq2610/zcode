#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, l, r;
    cin >> t;
    while (t--){
        cin >> n;
        int a[n];
        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }
        cin >> l >> r;
        int flag = 1;
        for(int j = l; j < r; ++j) {
            if(a[j+1] < a[j]){
                if (j+1==r){
                    break;
                } else{
                    for(int i = j+1; i <= r; ++i) {
                        if(a[i] < a[i+1]){
                            flag = 0;
                            break;
                        }
                    }
                }
            }
            if(!flag)
                break;
        }
        if (flag) cout << "Yes" << endl;
        else cout << "No" << endl;
    }
}
