//
// Created by ASUS on 30/11/2020.
//

#include<bits/stdc++.h>
using namespace std;

int gcd(int a, int b) {
    if (b == 0) return a;
    else return gcd(b, a % b);
}

void solve(int arr[], int d, int n) {
    d = d % n;
    int g_c_d = gcd(d, n);
    for (int i = 0; i < g_c_d; i++) {
        int temp = arr[i];
        int j = i;

        while (1) {
            int k = j + d;
            if (k >= n)
                k = k - n;

            if (k == i)
                break;

            arr[j] = arr[k];
            j = k;
        }
        arr[j] = temp;
    }
}

void print(int a[], long long n) {
    for (long long i = 0; i < n; i++)
        cout << a[i] << " ";
}


int main() {
    long long t, n, k;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        int a[n+1];
        for (long long i = 0; i < n; i++) cin >> a[i];
        solve(a, k, n);
        print(a, n);
        cout << endl;
    }
    return 0;
}