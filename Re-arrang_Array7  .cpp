//
// Created by ASUS on 30/11/2020.
//

#include<bits/stdc++.h>
using namespace std;

int myCompare(string X, string Y) {
    string XY = X.append(Y);
    string YX = Y.append(X);

    return XY.compare(YX) > 0 ? 1 : 0;
}

void printLargest(vector<string> arr) {
    sort(arr.begin(), arr.end(), myCompare);
    for (int i = 0; i < arr.size(); i++)
        cout << arr[i];
}


int main() {
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        string s[n+1];
        vector<string> arr;
        for (int i = 0; i < n; i++) {
            cin >> s[i];
            arr.push_back(s[i]);
        }
        printLargest(arr);
        cout << endl;
    }
    return 0;
}