//
// Created by ASUS on 07/12/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, m, i, j;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        int a[n+1], b[m+1];
        for (i = 0; i < n; i++) cin >> a[i];
        for (i = 0; i < m; i++) cin >> b[i];
        sort(a,a+n);
        int visited = a[0] - 1;
        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++) {
                if(a[j] == b[i]) {
                    cout << a[j] << " ";
                    a[j] = visited;
                }
                if (a[j] > b[i]) break;
            }
        }
        for (int i = 0; i < n; i++) {
            if (a[i] != visited) cout << a[i] << " ";
        }
        cout << endl;
    }
    return 0;
}