//
// Created by ASUS on 14/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

long long LCM(long long x, long long y, long long z) {
    long long res = (x * y) / __gcd(x, y);
    return (res * z) / __gcd(res, z);
}

long long findDivisible(long long n, long long x, long long y, long long z) {
    long long lcm = LCM(x, y, z);
    long long ndigitnumber = pow(10, n-1);
    long long reminder = ndigitnumber % lcm;
    if (reminder == 0) return ndigitnumber;
    else ndigitnumber += lcm - reminder;
    if (ndigitnumber < pow(10, n))
        return ndigitnumber;
    else return -1;
}
int main() {
    long long t, x, y, z, n;
    cin >> t;
    while (t--) {
        cin >> x >> y >> z >> n;
        cout << findDivisible(n, x, y, z);
        cout << endl;
    }
    return 0;
}