//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, a[100000], i, count;
    cin >> t;
    while (t--) {
        count = 0;
        cin >> n;
        for (i = 0; i < n; ++i) cin >> a[i];
        sort(a, a + n);
        for (i = 0; i < n; ++i) {
            if (a[i] - a[i-1] > 1) count += a[i] - a[i-1] - 1;
        }
        cout << count << endl;
    }
    return 0;
}