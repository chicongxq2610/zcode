//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n1, n2, n3, i, j;
    cin >> t;
    while (t--) {
        cin >> n1 >> n2 >> n3;
        long long a[n1], b[n2], c[n3], d[100000], k = 0;
        for (i = 0; i < n1; i++) cin >> a[i];
        for (i = 0; i < n2; i++) cin >> b[i];
        for (i = 0; i < n3; i++) cin >> c[i];
        i = 0, j = 0;
        while (i < n1 && j < n2) {
            if (a[i] < b[j]) i++;
            else if (a[i] > b[j]) j++;
            else {d[k++] = a[i];i++;j++;}
        }
        i = 0; j = 0;
        int count = 0;
        while (i < k && j < n3) {
            if (d[i] < c[j]) i++;
            else if (d[i] > c[j]) j++;
            else {
                cout << d[i] << " ";
                i++;j++;
                count++;
            }
        }
        if (!count) cout << -1;
        cout << endl;
    }
    return 0;
}