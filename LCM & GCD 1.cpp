#include<bits/stdc++.h>
using namespace std;

long long gcd(long long a, long long b) {
    if (a == 0) return b;
    return gcd(b % a, a);
}

int main() {
	long long t, a, b;
	cin>>t;
	while(t--) {
		cin>>a>>b;
		cout<<(a * b) / gcd(a, b)<<" "<<gcd(a,b);
	}
	return 0;
}

