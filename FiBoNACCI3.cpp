//
// Created by ASUS on 09/11/2020.
//

#include<bits/stdc++.h>
using namespace std;

bool check(int n) {
    int x = sqrt(n);
    return (x * x == n);
}

bool isFibonacci(int n) {
    return check(5 * n * n + 4) || check(5 * n * n - 4);
}

int main() {
    int t, n, a[101], i;
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n; i++) cin >> a[i];
        for (i = 0; i < n; i++) {
            if (isFibonacci(a[i])) cout << a[i] << " ";
        }
        cout << endl;
    }
    return 0;
}