//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

bool isPrime(int n) {
    if (n <= 1)
        return false;
    int i;
    for (i = 2; i <= sqrt(n); ++i)
        if (n % i == 0)
            return false;
    return true;
}

int main() {
    int t, x, count, i;
    cin >> t;
    while (t--) {
        cin >> x;
        count = 0;
        for (i = 1; i <= x; ++i) {
            if (__gcd(i, x) == 1) count++;
        }
        if (isPrime(count)) cout << 1;
        else cout << 0;
        cout << endl;
    }
    return 0;
}