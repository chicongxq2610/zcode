//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

long long a[1000000], b[1000000];

int main() {
    int t; long long n, m, i;
    long long int res;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < m; ++i) cin >> b[i];
        sort(a, a + n);
        sort(b, b + m);
        res = a[n - 1] * b[0];
        cout << res << endl;
    }
    return 0;
}