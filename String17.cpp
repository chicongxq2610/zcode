//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, i, j;
    string s;
    cin >> t;
    while (t--) {
        cin.ignore();
        cin >> s;
        int mark[26] = {0};
        int len = s.length(), count = 0;
        for (i = 0; i < len; ++i) {
            mark[s[i] - 'A']++;
        }
        for (i = 0; i < len; ++i) {
            if (mark[s[i] - 'A'] == 1) {
               cout << s[i];
            }
        }
        cout << endl;
    }
    return 0;
}