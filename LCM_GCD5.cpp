//
// Created by ASUS on 12/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t;
    long long a, x, y, i, g;
    cin >> t;
    while (t--) {
        cin >> a >> x >> y;
        g = __gcd(x, y);
        for (i = 0; i < g; ++i) cout << a;
        cout << endl;
    }
    return 0;
}