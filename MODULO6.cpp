//
// Created by ASUS on 20/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

long long aModM(string s, long long k) {
    long long res = 0;
    for (int i = 0; i < s.length(); ++i) {
        res = (res * 10 + (int)s[i] - '0') % k;
    }
    return res;
}

long long aPowBModM(string a, long long b, long long m) {
    long long x = aModM(a, m), res = 1;
    while (b > 0) {
        if (b & 1)
            res = (res * x) % m;
        b = b >> 1;
        x = (x * x) % m;
    }
    return res;
}

int main() {
    int t;
    long long b, m;
    string a;
    cin >> t;
    while (t--) {
        cin >> a >> b >> m;
        cout << aPowBModM(a, b, m) << endl;
    }
    return 0;
}