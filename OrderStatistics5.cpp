#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i;
    cin >> t;
    while (t--){
        cin >> n;
        int a[n+1];
        for (i = 0; i < n; i++) {
            cin >> a[i];
        }
        int max_diff = a[1] - a[0];
        int min_ele = a[0];
        int flag = 0;
        for (int i = 1; i < n; i++) {
            if (a[i] - min_ele > max_diff) {
                max_diff = a[i] - min_ele;
                flag = 1;
            }
            if (a[i] < min_ele) min_ele = a[i];
        }
        if (a[0] > a[1] && !flag) cout << -1;
        else cout << max_diff;
        cout << endl;
    }
}
