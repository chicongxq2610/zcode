//
// Created by ASUS on 15/01/2021.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, k, a[100000];
    cin >> t;
    while (t--) {
        cin >> n >> k;
        for (int i = 0; i < n; i++) cin >> a[i];
        sort(a, a+n);
        cout << a[k - 1] << endl;
    }
    return 0;
}