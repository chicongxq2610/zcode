//
// Created by ASUS on 09/10/2020.
//

#include<iostream>
using namespace std;

int main() {
    long long n, test, tong;
    cin >> test;
    while (test--) {
        cin>>n;
        tong = 0;
        while (n > 0 || tong > 9) {
            if (n == 0) {
                n = tong;
                tong = 0;
            }
            tong += n % 10;
            n /= 10;
        }
        cout<<tong<<endl;
    }
    return 0;
}