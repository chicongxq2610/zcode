//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, m, i, a[100000];
    cin >> t;
    while (t--) {
        cin >> n >> m;
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = m; i < n + m; ++i) cin >> a[i];
        sort(a, a + n + m);
        for (i = 0; i < n + m; ++i) cout << a[i] << " ";
        cout << endl;
    }
    return 0;
}