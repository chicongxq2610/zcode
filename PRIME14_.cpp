#include<bits/stdc++.h>
using namespace std;
int a[1000000];
void SieveOfEratosthenes() {
	for (int i = 2; i * i <= 1000000; i++) a[i] = 1;
	for (int i = 2; i * i <= 1000000; i++) {
		if (a[i]) {
			for (int j = i * i; j*j <= 1000000; j += i) a[j] = 0;	
		}
	}
}
int main() {
	int t;
	cin>>t;
	SieveOfEratosthenes();
	while(t--) {
		int n;
		cin>>n;
		int count = 2;
		while (count * count <= n) {
		if (a[count]) cout<<count * count<<" ";
		count++;
		}
		cout<<endl;
	}

	return 0;
}

