//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, i;
    string s;
    cin >> t;
    while (t--) {
        cin.ignore();
        cin >> s;
        int sum1 = 0, sum2 = 0;
        for (i = 0; i < s.length(); ++i) {
            if (i % 2 == 0) sum1 += s[i] - '0';
            else sum2 += s[i] - '0';
        }
        if ((sum1 - sum2 ) % 11 == 0) cout << 1;
        else cout << 0;
        cout << endl;
    }
    return 0;
}