#include<bits/stdc++.h>
using namespace std;

void printPrimeFactors(int n, int k) {
	int a[10000] = {0}, count = 0;
	while (n % 2 == 0) {
		a[count++] = 2;
		n = n / 2;
	}
	for (int i = 3; i <= sqrt(n); i += 2) {
		while (n % i == 0) {
			a[count++] = i;
			n = n / i;
		}
	}
	if (n > 2) a[count] = n;
	if (a[k-1] != 0) cout<<a[k-1];
	else cout<<"-1";
}

int main() {
	int t;
	cin>>t;
	while(t--) {
		int n, k;
		cin>>n>>k;
		printPrimeFactors(n, k);
		cout<<endl;
	}

	return 0;
}

