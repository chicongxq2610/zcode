//
// Created by ASUS on 07/12/2020.
//

#include<bits/stdc++.h>
using namespace std;

string solve(string s) {
    int len = s.length();
    int index = -1;
    for (int i = len - 2; i >= 0; i--) {
        if (s[i] > s[i+1]) {
            index = i;
            break;
        }
    }
    int k = -1;
    for (int i = len - 1; i > index; i--) {
        if (s[index] > s[i]) {
            if (k == -1) k = i;
            else if (s[i] >= s[k]) k = i;
        }
    }
    if (index == -1) return  "-1";
    if (k != -1) {
        swap(s[index], s[k]);
        return s;
    }
    return "-1";
}

int main() {
    int t;
    string s;
    cin >> t;
    while (t--) {
        cin >> s;
        cout << solve(s) << endl;
    }
    return 0;
}