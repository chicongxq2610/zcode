//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, j;
    cin >> t;
    while (t--) {
        cin >> n;
        long long a[n+1], b[n+1];
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < n; ++i) b[i] = -1;
        for (i = 0; i < n; ++i) {
            for (j = 0; j < n; ++j) {
                if (a[j] == i) b[i] = a[j];
            }
        }
        for (i = 0; i < n; ++i) {
            cout << b[i] <<" ";
        }
        cout << endl;
    }
    return 0;
}