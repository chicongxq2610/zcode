#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, k;
    cin >> t;
    while (t--){
        cin >> n >> k;
        int a[n+1];
        for (int i = 0; i < n; i++) {
            cin >> a[i];
        }
        int max = INT16_MIN, index;
        for (int i = 0; i < n - k + 1; i++) {
            int sum  = 0;
            for (int j = i; j < i+k; j++) {
                sum += a[j];
            }
            if (sum > max) {
                max = sum;
                index = i;
            }
        }
        for (int i = index; i < index + k; i++)
            cout << a[i] << " ";
        cout << endl;
    }
}
