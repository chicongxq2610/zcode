//
// Created by ASUS on 20/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

long long mod(string s, long long k) {
    long long res = 0;
    for (int i = 0; i < s.length(); ++i) {
        res = (res * 10 + (int)s[i] - '0') % k;
    }
    return res;
}

int main() {
    int t;
    long long k;
    string s;
    cin >> t;
    while (t--) {
        cin >> s >> k;
        cout << mod(s, k) << endl;
    }
    return 0;
}