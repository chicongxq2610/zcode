//
// Created by ASUS on 27/09/2020.
//

#include<bits/stdc++.h>
using namespace std;

void printArray(int arr[], int n) {
    int i;
    for (i = 0; i < n; ++i) {
        cout << arr[i];
    }
    cout << " ";
}

void generateBinaryStrings(int n, int arr[], int i) {
    if (i == n) {
        printArray(arr, n);
        return;
    }
    arr[i] = 0;
    generateBinaryStrings(n, arr, i + 1);
    arr[i] = 1;
    generateBinaryStrings(n, arr, i + 1);
}

int main() {
    int t, n, arr[21];
    cin >> t;
    while (t--) {
        cin >> n;
        generateBinaryStrings(n, arr, 0);
        cout << endl;
    }
    return 0;
}