//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int a[1000000];
void sieve() {
    for (int i = 2; i <= 1000000; i++) a[i] = 1;
    for (int i = 2; i * i <= 1000000; i++) {
        if (a[i]) {
            for (int j = i * i; j <= 1000000; j += i) a[j] = 0;
        }
    }
}

int main() {
    int t, n, i, j;
    cin >> t;
    sieve();
    while (t--) {
        cin >> n;
        int flag = 0;
        for (i = 2; i <= n - 2; i++) {
                if (a[i] && a[n-i]) {
                    cout << i << " " << n - i << endl;
                    flag = 1;
                    break;
                }
        }
        if (!flag) cout << -1 << endl;
    }
    return 0;
}