//
// Created by ASUS on 05/12/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, x, a[2001], i, p = 1000000007;
    cin >> t;
    while (t--) {
        long long s = 0;
        cin >> n >> x;
        for (i = 0; i < n; ++i) {
            cin >> a[i];
        }
        for (i = 0; i < n; ++i) {
            long long pow = 1;
            for (int j = 2; j <= n - i; j++) {
                pow = (pow * x) % p;
            }
            s += (a[i] * pow) % p;
        }
        cout << s % p<< endl;
    }
    return 0;
}