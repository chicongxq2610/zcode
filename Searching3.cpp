//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, a[100000];
    cin >> t;
    while (t--) {
        cin >> n;
        for (i = 0; i < n - 1; ++i) cin >> a[i];
        for (i = 0; i < n - 1; ++i) {
            if(a[i] - a[i-1] > 1) {
                cout << a[i-1] + 1 << endl;
                break;
            }
        }
    }
    return 0;
}