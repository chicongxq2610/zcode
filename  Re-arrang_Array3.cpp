#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, k;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        int a[n];
        int count = 0;
        for (int i = 0; i < n; i++){
            cin >> a[i];
            if (a[i] <= k) count++;
        }
        int dem=0;
        for (int i = 0; i < count; i++) {
            if (a[i] <= k) dem++;
        }
        int ans = dem;
        for (int i = count; i < n; i++) {
            if (a[i] <= k) dem++;
            if (a[i - count] <= k) dem--;
            ans = max(ans, dem);
        }
        cout << count - ans << endl;
    }
}