//
// Created by ASUS on 07/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int equipvalentBase4(string s) {
    if (s.compare("00") == 0) return 0;
    if (s.compare("01") == 0) return 1;
    if (s.compare("10") == 0) return 2;
    return 3;
}

void isDivisibleby5(string s) {
    int len = s.size();
    if (len % 2 != 0)
        s = '0' + s;
    int odd_sum, even_sum = 0;
    int isOddDigit = 1;
    for (int i = 0; i < s.size(); i += 2) {
        if (isOddDigit)
            odd_sum += equipvalentBase4(s.substr(i, 2));
        else even_sum += equipvalentBase4(s.substr(i, 2));
        isOddDigit ^= 1;
    }
    if (abs(odd_sum - even_sum) % 5 == 0) cout << "Yes";
    else cout << "No";
}

int main() {
    int t;
    string s;
    cin >> t;
    while (t--) {
        cin.ignore();
        cin >> s;
        isDivisibleby5(s);
        cout << endl;
    }
    return 0;
}