//
// Created by ASUS on 15/01/2021.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--) {
        string s;
        int mark[26] = {0};
        cin >> s;
        for (int i = 0; i < s.length(); i++) {
            mark[s[i] - 'a']++;
        }
        sort(mark, mark+26);
        if (mark[25] > s.length() / 2) cout << 0;
        else cout << 1;
        cout << endl;
    }
    return 0;
}