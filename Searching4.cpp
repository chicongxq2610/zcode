//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    long long t, n, x, i, a[100000];
    cin >> t;
    while (t--) {
        cin >> n >> x;
        for (i = 0; i < n; ++i) cin >> a[i];
        for (i = 0; i < n; ++i) {
            if (a[i] == x) {
                cout << i + 1 << endl;
                break;
            }
        }
    }
    return 0;
}