//
// Created by ASUS on 08/11/2020.
//

#include<bits/stdc++.h>
using namespace std;

bool visited[100] = {false};
vector<int> ke[100];

void DFS(int n) {
    if (visited[n]) return;
    visited[n] = true;
    for (int i = 0; i < ke[n].size(); i++)
        int k = ke[n][i];
        if (!visited[k]) {
            DFS(k);
        }
    }
}

int main() {
    int n, m, x, y;
    int c = 0;
    cin >> n >> m;
    for (int i = 0; i < m; i++) {
        cin >> x >> y;
        ke[x].push_back(y);
        ke[y].push_back(x);
    }
    for (int i = 1; i <= n; i++) {
        if (!visited[i]) {
            DFS(i);
            c++;
        }
    }
    cout << c;
    return 0;
}