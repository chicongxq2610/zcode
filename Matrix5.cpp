//
// Created by ASUS on 30/11/2020.
//

#include<bits/stdc++.h>
using namespace std;

int solve(int a[][100], int n) {
    int row[100] = {0}, col[100] = {0}, maxSum = 0, res = 0, i, j;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            row[i] += a[i][j];
            col[j] += a[i][j];
        }
    }

    for (i = 0; i < n; ++i) {
        maxSum = max(maxSum, row[i]);
        maxSum = max(maxSum, col[i]);
    }

    for (i = 0, j = 0; i < n && j < n;) {
        int m = min(maxSum - row[i], maxSum - col[j]);
        a[i][j] += m;
        row[i] += m;
        col[j] += m;
        res += m;
        if (row[i] == maxSum) ++i;
        if (col[j] == maxSum) ++j;
    }
    return res;
}

int main() {
    int t, n, a[100][100];
    cin >> t;
    while (t--) {
        cin >> n;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                cin >> a[i][j];
            }
        }
        cout << solve(a, n) << endl;
    }
    return 0;
}