//
// Created by ASUS on 15/01/2021.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--) {
        string s, s1 = "";
        cin >> s;
        int ans = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s[i] >= '0' && s[i] <= '9') s1 += s[i];
            else {
                if (s1.length() > 0) ans += stoi(s1);
                s1 = "";
            }
        }
        if (s1.length()) ans += stoi(s1);
        cout << ans << endl;
    }
    return 0;
}