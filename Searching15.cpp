//
// Created by ASUS on 05/10/2020.
//

#include<bits/stdc++.h>
using namespace std;

int main() {
    int t, n, i, j, k, x;
    cin >> t;
    while (t--) {
        cin >> n;
        int a[1000000];
        for (i = 0; i < n ; i++) {
            cin >> a[i];
        }
        cin >> k >> x;
        int flag = 0;
        for (i = 0; i < n; i++) {
            if (a[i] == x) {
                flag = 1;
                int count = 0;
                for (j = k/2; j > 0; j--) {
                    if (i + k/2 >= n)
                   if (i - j >= 0) {
                       cout << a[i - j] << " ";
                       count++;
                   }
                }
                for (j = 1; j <= k-count; j++) cout << a[i+j] << " ";
                break;
            }
        }
        if (!flag) {
            a[n] = x;
            sort(a, a+n+1);
            for (i = 0; i <= n; i++) {
                if (a[i] == x) {
                    for (j = k/2; j > 0; j--) {
                        cout << a[i - j] << " ";
                    }
                    for (j = 1; j <= k/2; j++) cout << a[i+j] << " ";
                    break;
                }
            }
        }
        cout << endl;
    }
    return 0;
}